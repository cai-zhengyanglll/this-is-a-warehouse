import allure


def setup_functio(): print("开始计算")


def teardown_function(): print("结束计算")


def teardown(): print("结束测试")


def addition(a, b):
    if type(a) not in [int, float] or type(b) not in [int, float]:
        return "Error: 参数类型错误"
    if not (-99 <= a <= 99) or not (-99 <= b <= 99):
        return "Error: 参数超出数据区间"
    return round(a + b, 2)


@allure.feature("计算器")
@allure.label("hebeu")
class TestAddition:

    @allure.story("整数相加")
    def test_integer_addition(self):
        with allure.step("输入两个整数，计算它们的和"):
            a, b = 8, 9
            result = addition(a, b)
        with allure.step("断言计算结果是否正确"):
            assert result == 17

    @allure.story("浮点数相加")
    def test_float_addition(self):
        with allure.step("输入两个浮点数，计算它们的和"):
            a, b = 5.23, 2.31
            result = addition(a, b)
        with allure.step("断言计算结果是否正确"):
            assert result == 7.54
