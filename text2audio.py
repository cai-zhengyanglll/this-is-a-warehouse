# file_name: test_allure_step.py
import allure
import requests


@allure.feature("冒烟测试")
class TestPetstore:
    def setup_class(self):
        # 基础的数据信息
        pass

    @allure.story("post")
    def test_post(self):
        with allure.step("发出查询接口请求"):
            r = requests.get("https://petstore.swagger.io/#/pet/addPet")
        with allure.step("获取查询接口响应"):
            print(r.json())
        with allure.step("查询接口断言"):
            assert r.status_code == 200

    @allure.story("get")
    def test_get(self):
        with allure.step("发出查询接口请求"):
            r = requests.get("https://petstore.swagger.io/#/pet/findPetsByStatus")
        with allure.step("获取查询接口响应"):
            print(r.json())
        with allure.step("查询接口断言"):
            assert r.status_code == 200
