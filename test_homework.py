import time

from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys


class TestHomework:
    def setup_method(self):
        self.driver = webdriver.Chrome()
        self.vars = {}

    def teardown_method(self):
        self.driver.quit()

    def test_homework(self):
        self.driver.get("https://ceshiren.com/")
        time.sleep(2)
        self.driver.set_window_size(912, 870)
        time.sleep(2)
        self.driver.find_element(By.CSS_SELECTOR, ".d-icon-search").click()
        time.sleep(2)
        element = self.driver.find_element(By.CSS_SELECTOR, ".d-icon-search")
        time.sleep(2)
        actions = ActionChains(self.driver)
        time.sleep(2)
        actions.move_to_element(element).perform()
        time.sleep(2)
        element = self.driver.find_element(By.CSS_SELECTOR, "body")
        time.sleep(2)
        actions = ActionChains(self.driver)
        time.sleep(2)
        actions.move_to_element(element).perform()
        time.sleep(2)
        self.driver.find_element(By.ID, "search-term").send_keys("20230524")
        time.sleep(2)
        self.driver.find_element(By.ID, "search-term").send_keys(Keys.ENTER)
        time.sleep(2)
        self.driver.find_element(By.CSS_SELECTOR, ".item:nth-child(1) .topic-title > span").click()
        time.sleep(2)
        assert self.driver.find_element(By.CSS_SELECTOR, "h3:nth-child(2)").text == "练习要求"
